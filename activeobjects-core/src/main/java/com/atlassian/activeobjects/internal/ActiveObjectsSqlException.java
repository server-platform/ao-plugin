package com.atlassian.activeobjects.internal;

import net.java.ao.ActiveObjectsException;
import net.java.ao.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.SQLTransientConnectionException;

public final class ActiveObjectsSqlException extends ActiveObjectsException {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private Database database;
    private Driver driver;

    public ActiveObjectsSqlException(EntityManager entityManager, SQLException cause) {
        super(cause);

        // If the cause is a connection-related exception, skip attempting to load metadata about the
        // database to avoid potentially exacerbating situations where the database pool is exhausted
        if (!isConnectionException(cause)) {
            getInformation(entityManager);
        }
    }

    public SQLException getSqlException() {
        return (SQLException) getCause();
    }

    @Override
    public String getMessage() {
        return "There was a SQL exception thrown by the Active Objects library:\n" + database + "\n" + driver + "\n\n" +
                super.getMessage();
    }

    private void getInformation(EntityManager entityManager) {
        try (Connection connection = entityManager.getProvider().getConnection()) {
            if (connection != null && !connection.isClosed()) {
                final DatabaseMetaData metaData = connection.getMetaData();
                database = getDatabase(metaData);
                driver = getDriver(metaData);
            }
        } catch (SQLException e) {
            logger.debug("Could not load database connection meta data", e);
            addSuppressed(e);
        }
    }

    private static Database getDatabase(DatabaseMetaData metaData) throws SQLException {
        return new Database(
                metaData.getDatabaseProductName(),
                metaData.getDatabaseProductVersion(),
                String.valueOf(metaData.getDatabaseMinorVersion()),
                String.valueOf(metaData.getDatabaseMajorVersion()));
    }

    private static Driver getDriver(DatabaseMetaData metaData) throws SQLException {
        return new Driver(
                metaData.getDriverName(),
                metaData.getDriverVersion());
    }

    private static boolean isConnectionException(SQLException e) {
        return e instanceof SQLNonTransientConnectionException || e instanceof SQLTransientConnectionException;
    }

    private static final class Database {
        private final String name;
        private final String version;
        private final String minorVersion;
        private final String majorVersion;

        Database(String name, String version, String minorVersion, String majorVersion) {
            this.name = name;
            this.version = version;
            this.minorVersion = minorVersion;
            this.majorVersion = majorVersion;
        }

        @Override
        public String toString() {
            return "Database:\n"
                    + "\t- name:" + name + "\n"
                    + "\t- version:" + version + "\n"
                    + "\t- minor version:" + minorVersion + "\n"
                    + "\t- major version:" + majorVersion;
        }
    }

    private static final class Driver {
        private final String name;
        private final String version;

        Driver(String name, String version) {
            this.name = name;
            this.version = version;
        }

        @Override
        public String toString() {
            return "Driver:\n"
                    + "\t- name:" + name + "\n"
                    + "\t- version:" + version;
        }
    }
}
