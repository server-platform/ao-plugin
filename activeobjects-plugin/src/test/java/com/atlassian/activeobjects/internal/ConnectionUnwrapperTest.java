package com.atlassian.activeobjects.internal;

import com.mchange.v2.c3p0.impl.NewProxyConnection;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Check different cases of how connections are expected to be unwrapped.
 */
public class ConnectionUnwrapperTest {

    @Test
    public void simpleUnwrapping() throws Exception {
        final Connection inner = mock(Connection.class);
        final DatabaseMetaData outerMetadata = mock(DatabaseMetaData.class);
        final Connection outer = mock(Connection.class);

        when(outer.getMetaData()).thenReturn(outerMetadata);
        when(outerMetadata.getConnection()).thenReturn(inner);

        assertThat(ConnectionUnwrapper.tryUnwrapConnection(outer), equalTo(Optional.of(inner)));
    }

    @Test
    public void unwrappingOfPooledC3P0Connections() throws Exception {
        final Connection inner = mock(Connection.class);
        final Connection pooled = createPooledConnection(inner);
        final DatabaseMetaData outerMetadata = mock(DatabaseMetaData.class);
        final Connection outer = mock(Connection.class);

        when(outer.getMetaData()).thenReturn(outerMetadata);
        when(outerMetadata.getConnection()).thenReturn(pooled);

        assertThat(ConnectionUnwrapper.tryUnwrapConnection(outer), equalTo(Optional.of(inner)));
    }

    private Connection createPooledConnection(Connection inner) {
        try {
            final Constructor<NewProxyConnection> constructor
                    = NewProxyConnection.class.getDeclaredConstructor(Connection.class);
            constructor.setAccessible(true);
            return constructor.newInstance(inner);
        } catch (Exception e) {
            throw new RuntimeException("Wasn't able to create pooled connection. Please check what went wrong and " +
                    "fix the code to create an instance of NewPooledConnection with specific inner connection");
        }
    }

}