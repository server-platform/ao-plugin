package com.atlassian.activeobjects.internal;

import com.atlassian.activeobjects.ActiveObjectsPluginException;
import com.atlassian.activeobjects.config.ActiveObjectsConfiguration;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.activeobjects.spi.DatabaseType;
import com.atlassian.activeobjects.spi.TenantAwareDataSourceProvider;
import com.atlassian.activeobjects.spi.TransactionSynchronisationManager;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.tenancy.api.Tenant;
import net.java.ao.EntityManager;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toList;

/**
 * Creates a new instance of ActiveObjects given a dataSourceProvider
 */
public final class DataSourceProviderActiveObjectsFactory extends AbstractActiveObjectsFactory {
    private final EntityManagerFactory entityManagerFactory;
    private final TenantAwareDataSourceProvider tenantAwareDataSourceProvider;

    private TransactionSynchronisationManager transactionSynchronizationManager;

    public DataSourceProviderActiveObjectsFactory(ActiveObjectUpgradeManager aoUpgradeManager,
                                                  EntityManagerFactory entityManagerFactory, TenantAwareDataSourceProvider tenantAwareDataSourceProvider,
                                                  TransactionTemplate transactionTemplate, ClusterLockService clusterLockService) {
        super(DataSourceType.APPLICATION, aoUpgradeManager, transactionTemplate, clusterLockService);
        this.entityManagerFactory = checkNotNull(entityManagerFactory);
        this.tenantAwareDataSourceProvider = checkNotNull(tenantAwareDataSourceProvider);
    }

    public void setTransactionSynchronizationManager(TransactionSynchronisationManager transactionSynchronizationManager) {
        this.transactionSynchronizationManager = transactionSynchronizationManager;
    }

    /**
     * Creates an {@link com.atlassian.activeobjects.external.ActiveObjects} using the
     * {@link com.atlassian.activeobjects.spi.TenantAwareDataSourceProvider}
     *
     * @param configuration the configuration of active objects
     * @return a new configured, ready to go ActiveObjects instance
     * @throws ActiveObjectsPluginException if the data source obtained from the {@link com.atlassian.activeobjects.spi.TenantAwareDataSourceProvider}
     *                                      is {@code null}
     */
    @Override
    protected ActiveObjects doCreate(final ActiveObjectsConfiguration configuration, final Tenant tenant) {
        return transactionTemplate.execute((TransactionCallback<ActiveObjects>) () -> {
            final DataSource dataSource = getDataSource(tenant);
            final DatabaseType dbType = getDatabaseType(tenant);
            final EntityManager entityManager = entityManagerFactory.getEntityManager(dataSource, dbType,
                    tenantAwareDataSourceProvider.getSchema(tenant), configuration);
            return new EntityManagedActiveObjects(entityManager, new SalTransactionManager(transactionTemplate,
                    entityManager, transactionSynchronizationManager), dbType);
        });
    }

    @Override
    protected void upgrade(final ActiveObjectsConfiguration configuration, final Tenant tenant) {
        DatabaseType dbType = getDatabaseType(tenant);
        List<ActiveObjectsUpgradeTask> upgradeTasks = configuration.getUpgradeTasks();
        if (dbType == DatabaseType.POSTGRESQL || dbType == DatabaseType.UNKNOWN) {
            DataSource dataSource = getDataSource(tenant);
            upgradeTasks = upgradeTasks.stream()
                    .map(task -> new PostgresActiveObjectsUpgradeTask(dataSource, task))
                    .collect(toList());
        }
        aoUpgradeManager.upgrade(configuration.getTableNamePrefix(), upgradeTasks,
                () -> doCreate(configuration, tenant));
    }

    private DataSource getDataSource(final Tenant tenant) {
        final DataSource dataSource = tenantAwareDataSourceProvider.getDataSource(tenant);
        if (dataSource == null) {
            throw new ActiveObjectsPluginException("No data source defined in the application");
        }
        return new ActiveObjectsDataSource(dataSource);
    }

    private DatabaseType getDatabaseType(final Tenant tenant) {
        final DatabaseType databaseType = tenantAwareDataSourceProvider.getDatabaseType(tenant);
        if (databaseType == null) {
            throw new ActiveObjectsPluginException("No database type defined in the application");
        }
        return databaseType;
    }

    public static class ActiveObjectsDataSource implements DataSource {
        private final DataSource dataSource;

        ActiveObjectsDataSource(DataSource dataSource) {
            this.dataSource = dataSource;
        }

        @Override
        public Connection getConnection() throws SQLException {
            return dataSource.getConnection();
        }

        @Override
        public Connection getConnection(String username, String password) {
            throw new IllegalStateException("Not allowed to get a connection for non default username/password");
        }

        /**
         * Returns 0, indicating to use the default system timeout.
         */
        @Override
        public int getLoginTimeout() {
            return 0;
        }

        /**
         * Setting a login timeout is not supported.
         */
        @Override
        public void setLoginTimeout(int timeout) {
            throw new UnsupportedOperationException("setLoginTimeout");
        }

        /**
         * LogWriter methods are not supported.
         */
        @Override
        public PrintWriter getLogWriter() {
            throw new UnsupportedOperationException("getLogWriter");
        }

        /**
         * LogWriter methods are not supported.
         */
        @Override
        public void setLogWriter(PrintWriter pw) {
            throw new UnsupportedOperationException("setLogWriter");
        }

        @Override
        public <T> T unwrap(Class<T> tClass) {
            throw new UnsupportedOperationException("unwrap");
        }

        @Override
        public boolean isWrapperFor(Class<?> aClass) {
            throw new UnsupportedOperationException("isWrapperFor");
        }

        // @Override Java 7 only
        public Logger getParentLogger() throws SQLFeatureNotSupportedException {
            throw new SQLFeatureNotSupportedException();
        }
    }

    private static class PostgresActiveObjectsUpgradeTask implements ActiveObjectsUpgradeTask {

        private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PostgresActiveObjectsUpgradeTask.class);

        private final DataSource dataSource;
        private final ActiveObjectsUpgradeTask delegate;

        private PostgresActiveObjectsUpgradeTask(DataSource dataSource, ActiveObjectsUpgradeTask delegate) {
            this.dataSource = dataSource;
            this.delegate = delegate;
        }

        @Override
        public ModelVersion getModelVersion() {
            return delegate.getModelVersion();
        }

        @Override
        public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {
            boolean upgraded = configureConnectionAndRun(currentVersion, ao);
            if (!upgraded) {
                //Something went wrong trying to configure the connection before running the upgrade task
                //Try to execute the upgrade with the connection as is, it might still work.
                delegate.upgrade(currentVersion, ao);
            }
        }

        /**
         * Postgres can use server side prepared statements to optimise query execution. The Postgres driver
         * will use server side prepared statements when the {@code prepareThreshold} is exceeded. Unfortunately if
         * the table is queried with server side prepared statements in the same transaction in which
         * the schema is modified the DB server will fail with the message: <i>"cached plan must not change result type"</i>.
         * This is triggered by AO during upgrade tasks because the table is queried to "read the schema" (see
         * {@link net.java.ao.DatabaseProvider#renderMetadataQuery}).
         *
         * Here we attempt to disable server side prepared statements so that plugins with several upgrade tasks
         * migrating the same table will be able to avoid this error.
         *
         * @param currentVersion the current version of the model currently in the database.
         * @param ao             a configured instance of the Active Objects, which is not associated with any entity (yet).
         * @return {@code true} if the upgrade task has been executed. {@code false} otherwise
         */
        private boolean configureConnectionAndRun(final ModelVersion currentVersion, final ActiveObjects ao) {
            try (Connection c = dataSource.getConnection()) {
                // Try to get the most nested connection.
                Connection connection = ConnectionUnwrapper.tryUnwrapConnection(c).orElse(c);

                int previousPrepareThreshold;
                try {
                    //PGConnection is an interface defining 'getPrepareThreshold' so the method will be public
                    Method getPrepareThreshold = connection.getClass().getMethod("getPrepareThreshold");
                    previousPrepareThreshold = (int) getPrepareThreshold.invoke(connection);
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    logger.warn("Failed to retrieve prepare threshold", e);
                    return false;
                }

                if (previousPrepareThreshold == 0) {
                    //Server side prepare statements have already been disabled. Nothing to do here.
                    return false;
                }

                Method setPrepareThreshold;
                try {
                    setPrepareThreshold = connection.getClass().getMethod("setPrepareThreshold", int.class);
                    setPrepareThreshold.invoke(connection, 0);
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    logger.warn("Failed to disable server side prepared statements", e);
                    return false;
                }

                try {
                    delegate.upgrade(currentVersion, ao);
                } finally {
                    try {
                        setPrepareThreshold.invoke(connection, previousPrepareThreshold);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        logger.warn("Failed to reset server side prepared statements", e);
                    }
                }
                return true;
            } catch (SQLException e) {
                logger.warn("Failed to retrieve connection", e);
                return false;
            }
        }
    }
}
