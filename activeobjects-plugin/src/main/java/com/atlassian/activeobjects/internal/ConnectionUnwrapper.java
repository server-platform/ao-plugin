package com.atlassian.activeobjects.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

/**
 * This utility class is responsible for unwrapping the connection proxies and wrappers to get to the
 * DBMS specific connection underneath (e.g. {@code PGConnection}).
 * <p>
 * It might be used to access some methods exclusive for some DBMS connections.
 *
 * @since 3.0.3
 */
public final class ConnectionUnwrapper {

    private static final Logger log = LoggerFactory.getLogger(ConnectionUnwrapper.class);

    private ConnectionUnwrapper() {
    }

    /**
     * Try to extract the most nested connection from the possible wrapped passed connection {@code origin}. Since
     * the task is impossible to solve in general case (it depends on implementation of {@code Connection}), this method
     * provides only best effort guarantees and doesn't guarantee any particular result (there's no clear definition of
     * success). Please check the return value and handle the errors gracefully.
     *
     * @return The most nested connection that we could get. {@code Optional.empty()} if we couldn't extract anything
     * at all.
     */
    @Nonnull
    public static Optional<Connection> tryUnwrapConnection(@Nonnull Connection origin) {
        try {
            // Unwrap any proxies applied to the Connection to get the real one (hopefully).
            final Connection withoutProxies = origin.getMetaData().getConnection();

            // Handle the case when the connection is a part of c3p0 pool. This branch is covering Confluence Server.
            if (withoutProxies.getClass().getName().equals("com.mchange.v2.c3p0.impl.NewProxyConnection")) {
                // For this class we know that the real connection is stored in protected field "inner"
                try {
                    final Field inner = withoutProxies.getClass().getDeclaredField("inner");
                    inner.setAccessible(true);
                    return Optional.ofNullable((Connection) inner.get(withoutProxies));
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    log.warn("Wasn't able to unwrap NewProxyConnection", e);
                    return Optional.of(withoutProxies);
                }
            }

            // This is the best connection we can get, so just return it.
            return Optional.of(withoutProxies);
        } catch (SQLException e) {
            log.warn("Couldn't unwrap the connection {}", origin, e);
            return Optional.empty();
        }
    }

}
