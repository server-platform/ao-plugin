package com.atlassian.activeobjects.config;

import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.internal.DataSourceType;
import com.atlassian.activeobjects.internal.Prefix;
import net.java.ao.RawEntity;
import net.java.ao.SchemaConfiguration;
import net.java.ao.schema.NameConverters;

import java.util.List;
import java.util.Set;

/**
 * This represents the configuration of active objects for a given module descriptor.
 */
public interface ActiveObjectsConfiguration {
    String AO_TABLE_PREFIX = "AO";

    /**
     * The plugin key for which this configuration is defined.
     *
     * @return a {@link PluginKey}, cannot be {@code null}
     */
    PluginKey getPluginKey();

    /**
     * The datasource type that this active objects is meant to use.
     *
     * @return a valid DataSourceType
     */
    DataSourceType getDataSourceType();

    /**
     * The prefix to use for table names in the database
     *
     * @return the prefix to use for table names in the database.
     */
    Prefix getTableNamePrefix();

    /**
     * Gets the name converters to use with Active Objects
     *
     * @return the name converters
     */
    NameConverters getNameConverters();

    /**
     * Gets the schema configuration to use with Active Objects
     *
     * @return a schema configuration
     */
    SchemaConfiguration getSchemaConfiguration();

    /**
     * The set of 'configured' entities for the active objects configuration.
     *
     * @return a set of entity classes, empty of no entities have been defined.
     */
    Set<Class<? extends RawEntity<?>>> getEntities();

    /**
     * Gets the upgrade tasks associated with Active Objects
     *
     * @return the upgrade tasks
     */
    List<ActiveObjectsUpgradeTask> getUpgradeTasks();
}
