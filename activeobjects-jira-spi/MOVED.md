This plugin was moved into the Jira repo as `jira-components/jira-plugins/jira-activeobjects-spi-plugin` by
[this PR](https://stash.atlassian.com/projects/JIRA/repos/jira/pull-requests/1358/overview).
