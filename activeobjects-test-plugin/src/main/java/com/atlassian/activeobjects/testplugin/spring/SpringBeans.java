package com.atlassian.activeobjects.testplugin.spring;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.TransactionalAnnotationProcessor;
import com.atlassian.activeobjects.spi.Backup;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * The Spring bean definitions for the ActiveObjects Test Plugin.
 */
@Configuration
public class SpringBeans {

    @Bean
    ActiveObjects activeObjects() {
        return importOsgiService(ActiveObjects.class);
    }

    @Bean
    Backup spiBackup() {
        return importOsgiService(Backup.class);
    }

    @Bean
    TransactionalAnnotationProcessor transactionalAnnotationProcessor(ActiveObjects ao) {
        return new TransactionalAnnotationProcessor(ao);
    }
}
