package com.atlassian.activeobjects.confluence.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * The Spring bean definitions for the ActiveObjects Confluence SPI Plugin.
 *
 * This class simply aggregates the {@link Configuration} classes imported below.
 */
@Configuration
@Import({
        ImportBeans.class,
        PluginBeans.class,
        ExportBeans.class
})
public class SpringBeans {
    /* empty */
}
