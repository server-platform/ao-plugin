package com.atlassian.activeobjects.confluence.spring;

import com.atlassian.activeobjects.spi.TenantAwareDataSourceProvider;
import com.atlassian.confluence.core.SynchronizationManager;
import com.atlassian.hibernate.PluginHibernateSessionFactory;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * The Spring bean definitions for the ActiveObjects Confluence SPI Plugin.
 */
@Configuration
public class ImportBeans {
    @Bean
    PluginHibernateSessionFactory pluginHibernateSessionFactory() {
        return importOsgiService(PluginHibernateSessionFactory.class);
    }

    @Bean
    SynchronizationManager synchronizationManager() {
        return importOsgiService(SynchronizationManager.class);
    }

    @Bean
    TenantAwareDataSourceProvider tenantAwareDataSourceProvider() {
        return importOsgiService(TenantAwareDataSourceProvider.class);
    }

    @Bean
    ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory() {
        return importOsgiService(ThreadLocalDelegateExecutorFactory.class);
    }
}
