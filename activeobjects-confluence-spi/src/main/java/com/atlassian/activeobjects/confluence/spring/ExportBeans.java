package com.atlassian.activeobjects.confluence.spring;

import com.atlassian.activeobjects.spi.CompatibilityTenantContext;
import com.atlassian.activeobjects.spi.InitExecutorServiceProvider;
import com.atlassian.activeobjects.spi.TenantAwareDataSourceProvider;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

/**
 * The Spring bean definitions for the exported OSGi services..
 */
@Configuration
@ParametersAreNonnullByDefault
public class ExportBeans {
    @Bean
    FactoryBean<ServiceRegistration> exportCompatibilityTenantContext(
            CompatibilityTenantContext compatibilityTenantContext) {
        return exportOsgiService(compatibilityTenantContext, as(CompatibilityTenantContext.class));
    }

    @Bean
    FactoryBean<ServiceRegistration> exportInitExecutorServiceProvider(
            InitExecutorServiceProvider initExecutorServiceProvider) {
        return exportOsgiService(initExecutorServiceProvider, as(InitExecutorServiceProvider.class));
    }

    @Bean
    FactoryBean<ServiceRegistration> exportTenantAwareDataSourceProvider(
            final TenantAwareDataSourceProvider tenantAwareDataSourceProvider) {
        return exportOsgiService(tenantAwareDataSourceProvider, as(TenantAwareDataSourceProvider.class));
    }
}
