package com.atlassian.activeobjects.confluence.spring;

import com.atlassian.activeobjects.confluence.ConfluenceInitExecutorServiceProvider;
import com.atlassian.activeobjects.confluence.ConfluenceTenantAwareDataSourceProvider;
import com.atlassian.activeobjects.confluence.hibernate.DialectExtractor;
import com.atlassian.activeobjects.confluence.hibernate.HibernateSessionDialectExtractor;
import com.atlassian.activeobjects.confluence.transaction.ConfluenceAOSynchronisationManager;
import com.atlassian.activeobjects.spi.CompatibilityTenantContext;
import com.atlassian.activeobjects.spi.CompatibilityTenantContextImpl;
import com.atlassian.activeobjects.spi.DefaultInitExecutorServiceProvider;
import com.atlassian.activeobjects.spi.InitExecutorServiceProvider;
import com.atlassian.activeobjects.spi.TenantAwareDataSourceProvider;
import com.atlassian.activeobjects.spi.TransactionSynchronisationManager;
import com.atlassian.confluence.core.SynchronizationManager;
import com.atlassian.hibernate.PluginHibernateSessionFactory;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.tenancy.api.TenantAccessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * The Spring bean definitions for the ActiveObjects Confluence SPI Plugin.
 */
@Configuration
@ParametersAreNonnullByDefault
public class PluginBeans {
    @Bean
    CompatibilityTenantContext compatibilityTenantContext(
            final TenantAccessor tenantAccessor) {
        return new CompatibilityTenantContextImpl(tenantAccessor);
    }

    @Bean
    DefaultInitExecutorServiceProvider defaultInitExecutorServiceProvider(
            final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory) {
        return new DefaultInitExecutorServiceProvider(threadLocalDelegateExecutorFactory);
    }

    @Bean
    DialectExtractor dialectExtractor(
            final PluginHibernateSessionFactory sessionFactory,
            final TransactionTemplate transactionTemplate) {
        return new HibernateSessionDialectExtractor(sessionFactory, transactionTemplate);
    }

    @Bean
    InitExecutorServiceProvider initExecutorServiceProvider(
            final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory,
            final TenantAwareDataSourceProvider tenantAwareDataSourceProvider,
            final InitExecutorServiceProvider defaultInitExecutorServiceProvider) {
        return new ConfluenceInitExecutorServiceProvider(
                threadLocalDelegateExecutorFactory,
                tenantAwareDataSourceProvider,
                defaultInitExecutorServiceProvider);
    }

    @Bean
    TenantAwareDataSourceProvider tenantAwareDataSourceProvider(
            final PluginHibernateSessionFactory sessionFactory,
            final DialectExtractor dialectExtractor) {
        return new ConfluenceTenantAwareDataSourceProvider(sessionFactory, dialectExtractor);
    }

    @Bean
    TransactionSynchronisationManager transactionSynchronisationManager(
            final SynchronizationManager synchManager) {
        return new ConfluenceAOSynchronisationManager(synchManager);
    }
}
