package com.atlassian.dbexporter.node.stax;

import com.atlassian.dbexporter.ImportExportErrorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.xml.bind.DatatypeConverter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class StaxStreamWriterTest {
    @Mock
    private ImportExportErrorService errorService;
    private Writer output;

    private StaxStreamWriter staxStreamWriter;

    @Before
    public void setUp() {
        output = new StringWriter();
        staxStreamWriter = new StaxStreamWriter(errorService, output, Charset.forName("utf-8"), "");

    }

    @Test
    public void nodeCreatorShouldEncodeBinaryAsBase64() {
        final byte[] bytes = new byte[]{0, 1, 100, 2};

        staxStreamWriter.addRootNode("root").setContentAsBinary(bytes);
        staxStreamWriter.close();

        assertThat(output.toString(), containsString(DatatypeConverter.printBase64Binary(bytes)));
    }
}
